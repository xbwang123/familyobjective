import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_family/common/cloud_api.dart';
import 'package:my_family/common/global.dart';
import 'package:my_family/common/global_event.dart';
import 'package:my_family/common/http_util.dart';
import 'package:my_family/common/routers.dart';
import 'package:my_family/models/member.dart';
import 'package:my_family/models/user.dart';
import 'package:my_family/states/user_model.dart';
import 'package:my_family/widgets/dialog.dart';
import 'package:my_family/widgets/list_memu_item.dart';
import 'package:my_family/widgets/loading_dlg.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';

class SettingPage extends StatefulWidget {
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  var _eventSubscription;
  String version = "";

  @override
  void initState() {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        version = packageInfo.version;
      });
      version = packageInfo.version;
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _eventSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('设置'),
        actions: <Widget>[
          FlatButton.icon(
            onPressed: () async {
              showConfirmDialog(context, '确定要退出吗', () {
                Provider.of<UserModel>(context, listen: false).user = null;
                exit(0);
              });
            },
            //backgroundColor: Colors.green,
            label: Text(''),
            icon: Icon(
              Icons.exit_to_app,
            ),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          _picture(),
          _themeColor(),
          _memberSetting(),
          _familyAvatar(),
          _downloadPage(),
        ],
      ),
      floatingActionButton: _buildVersion(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }

  Widget _buildVersion(context) {
    return FlatButton(
      child: Text('版本: v' + version, style:TextStyle(color: Colors.black26)),
      onPressed: (){},
    );
  }
  Widget _picture() {
    var avatar = Global.profile.user.familyAvatar;
    if (avatar != null && avatar.length > 10) {
      return CachedNetworkImage(
        imageUrl: avatar,
        fit: BoxFit.fill,
        width: ScreenUtil().setWidth(750),
        placeholder: (context, url) => CircularProgressIndicator(),
        errorWidget: (context, url, error) => Icon(Icons.error),
      );
    } else {
      return Image.asset('images/family.jpg');
    }
  }

  Widget _themeColor() {
    return buildListMenuItem(context, Icons.color_lens, '主题颜色', () {
      Routers.router
          .navigateTo(context, Routers.themeSettingPage, replace: false);
    });
  }

  Widget _memberSetting() {
    return buildListMenuItem(context, Icons.group, '家庭成员', () {
      if (Global.isAdult()) {
        Routers.router
            .navigateTo(context, Routers.memberSettingPage, replace: false);
      } else {
        Fluttertoast.showToast(msg: '小朋友不要乱点', gravity: ToastGravity.CENTER);
      }
    });
  }

  Widget _familyAvatar() {
    return buildListMenuItem(context, Icons.picture_in_picture, '背景图片', () {
      _selectFamilyAvatar();
    });
  }

  Widget _downloadPage() {
    return buildListMenuItem(context, Icons.insert_drive_file, '软件说明', () {
      Routers.router.navigateTo(context, Routers.downloadPage, replace: false);
    });
  }

  Future _selectFamilyAvatar() async {
    var imageFile = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 480, maxHeight: 720);
    if(imageFile == null) {
      return;
    }
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: imageFile.path,
        aspectRatioPresets: Platform.isAndroid
            ? [CropAspectRatioPreset.ratio16x9]
            : [CropAspectRatioPreset.ratio16x9],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: '剪切图片',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.ratio16x9,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
            title: '剪切图片',
            rectX: 0.0,
            rectY: 0.0,
            rectWidth: 640.0,
            rectHeight: 360.0,
            cancelButtonTitle: '取消',
            doneButtonTitle: '确定'));
    if (croppedFile != null) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return new LoadingDialog(
              text: "正在保存...",
            );
          });
      FormData formData =  FormData.fromMap({});
      formData.files.add(MapEntry(
            "file",
            MultipartFile.fromFileSync(croppedFile.path, filename: "aa.png"),
          ));
      print(formData.toString());
      HttpUtil.getInstance()
          .post(
              "/api/v1/ums/family/avatar/" +
                  Global.profile.user.familyId.toString(),
              formData: formData)
          .then((val) {
        Navigator.pop(context);
        print(val);
        if (val['code'] == '10000') {
          Fluttertoast.showToast(msg: '设置成功', gravity: ToastGravity.CENTER);
          User user = Global.profile.user;
          user.familyAvatar = val['data'];
          Provider.of<UserModel>(context, listen: false).user = user;
          setState(() {
            
          });
        } else {
          Fluttertoast.showToast(
              msg: val['message'], gravity: ToastGravity.CENTER);
        }
      });
    }
  }
}
