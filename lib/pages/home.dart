import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_family/common/global_event.dart';
import 'package:my_family/pages/home_family.dart';
import 'package:my_family/pages/home_forum.dart';

import 'package:my_family/pages/home_index.dart';
import 'package:my_family/pages/home_setting.dart';
import 'package:my_family/pages/login.dart';
import 'package:my_family/widgets/dialog.dart';
import 'package:my_family/widgets/upgrade.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  List<Widget> list = List();
  var _eventSubscription;

  @override
  void initState() {
    list..add(IndexPage())..add(FamilyPage())..add(HomeForumPage())..add(SettingPage());
    _eventSubscription =
        GlobalEventBus().event.on<CommonEventWithType>().listen((event) {
      print("C onEvent:" + event.eventType);
      if (event.eventType == EVENT_TOKEN_ERROR) {
        Navigator.of(context).pushAndRemoveUntil(
            new MaterialPageRoute(builder: (context) => new LoginPage()),
            (route) => route == null);
      }
    });
    super.initState(); //无名无参需要调用
    CheckUpdate().check(context);
  }

  @override
  void dispose() {
    super.dispose();
    _eventSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 750, height: 1334);
    return Scaffold(
      body: list[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.my_location,
            ),
            title: Text('目标'),
          ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.group,
              ),
              title: Text('成员'),
              ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.chat,
              ),
              title: Text('交流'),
              ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.settings,
            ),
            title: Text('设置'),
          ),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        onTap: (int index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
     
    );
  }

  Future<bool> showInstallUpdateDialog() {
    return showDialog<bool>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("检测到新版本"),
          content: Text("已准备好更新，确认安装新版本?"),
          actions: <Widget>[
            FlatButton(
              child: Text(
                "取消",
                style: TextStyle(color: Color(0xff999999)),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              } // 关闭对话框
            ),
            FlatButton(
              child: Text("确认"),
              onPressed: () {
                //关闭对话框并返回true
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
  }

 
}
