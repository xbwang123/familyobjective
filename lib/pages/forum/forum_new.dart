import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_family/common/global.dart';
import 'package:my_family/common/global_event.dart';
import 'package:my_family/common/http_util.dart';
import 'package:my_family/models/member.dart';

class ForumNewPage extends StatefulWidget {
  @override
  _ForumNewPageState createState() => _ForumNewPageState();
}

class _ForumNewPageState extends State<ForumNewPage> {
  final TextEditingController _titleController =
      TextEditingController.fromValue(TextEditingValue(text: ''));
  final TextEditingController _contentController =
      TextEditingController.fromValue(TextEditingValue(text: ''));

  List<String> _chips = <String>['问题反馈', '心得体会'];
  String category = '问题反馈';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('交流反馈'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _buildInputTitle(),
            _buildInputContent(),
            _buildStartDay(),
            Container(
              height: ScreenUtil().setHeight(100),
              child: Text(''),
            )
          ],
        ),
      ),
      floatingActionButton: _buildFloatingActionButtion(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget _buildInputTitle() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(Icons.title, color: Theme.of(context).accentColor),
              Text(
                ' 标题',
                style: TextStyle(fontWeight: FontWeight.w800),
              )
            ],
          ),
          TextField(
            keyboardType: TextInputType.text,
            controller: _titleController,
            decoration: InputDecoration(hintText: '留下标题'),
            maxLines: 1,
          )
        ],
      ),
    );
  }

  Widget _buildInputContent() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(Icons.message, color: Theme.of(context).accentColor),
              Text(
                ' 内容',
                style: TextStyle(fontWeight: FontWeight.w800),
              )
            ],
          ),
          TextField(
            keyboardType: TextInputType.text,
            controller: _contentController,
            decoration: InputDecoration(hintText: '写下内容'),
            maxLines: 5,
          )
        ],
      ),
    );
  }

  Iterable<Widget> get chipWidgets sync* {
    for (String chip in _chips) {
      yield Padding(
        padding: EdgeInsets.all(10),
        child: ChoiceChip(
          backgroundColor: Colors.black12,
          label: Text(chip),
          labelStyle: TextStyle(fontWeight: FontWeight.bold),
          labelPadding: EdgeInsets.only(left: 10, right: 10),
          onSelected: (val) {
            setState(() {
              category = val.toString();
            });
          },
          selectedColor: Theme.of(context).accentColor,
          selected: category == chip,
        ),
      );
    }
  }

  Widget _buildStartDay() {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(Icons.category, color: Theme.of(context).accentColor),
              Text(
                ' 类别',
                style: TextStyle(fontWeight: FontWeight.w800),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 0),
            child: Row(
              children: chipWidgets.toList(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFloatingActionButtion(context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
      width: ScreenUtil().setWidth(750),
      height: ScreenUtil().setHeight(80),
      child: RaisedButton(
        child: Text(
          '提交',
          style: TextStyle(color: Colors.white, fontSize: 18),
        ),
        color: Theme.of(context).primaryColor,
        onPressed: () {
          _submit();
        },
      ),
    );
  }

  _submit() {
    String title = _titleController.text;
    if (title.length < 4) {
      Fluttertoast.showToast(msg: '请输入标题', gravity: ToastGravity.CENTER);
      return;
    }

    String content = _contentController.text;
    if (content.length < 4) {
      Fluttertoast.showToast(msg: '请输入内容', gravity: ToastGravity.CENTER);
      return;
    }

    var formData = {
      "category": category,
      "content": content,
      "likes": 0,
      "title": title,
      "userId": Global.profile.user.userId,
      "userAvatar": Global.profile.user.avatar,
      "userNick": Global.profile.user.nick
    };
    HttpUtil.getInstance()
        .post("api/v1/forum/forumInfo", formData: formData)
        .then((val) {
      print(val);
      if (val['code'] == '10000') {
        Fluttertoast.showToast(msg: '提交成功', gravity: ToastGravity.CENTER);
        GlobalEventBus.fireRefreshForumList();
        Navigator.of(context).pop();
      } else {
        Fluttertoast.showToast(
            msg: val['message'], gravity: ToastGravity.CENTER);
      }
    });
  }
}
