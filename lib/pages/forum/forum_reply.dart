import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_family/common/global.dart';
import 'package:my_family/common/global_event.dart';
import 'package:my_family/common/http_util.dart';
import 'package:my_family/models/member.dart';

class ForumReplyPage extends StatefulWidget {
  @override
  _ForumReplyPageState createState() => _ForumReplyPageState();
}

class _ForumReplyPageState extends State<ForumReplyPage> {

  final TextEditingController _contentController =
      TextEditingController.fromValue(TextEditingValue(text: ''));
  String _forumId;
  String _title;
  
  @override
  void initState() {
    _title = Global.prefs.getString("_title");
    _forumId = Global.prefs.getString("_forumId");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _buildInputContent(),
  
            Container(
              height: ScreenUtil().setHeight(100),
              child: Text(''),
            )
          ],
        ),
      ),
      floatingActionButton: _buildFloatingActionButtion(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget _buildInputContent() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(Icons.message, color: Theme.of(context).accentColor),
              Text(
                ' 内容',
                style: TextStyle(fontWeight: FontWeight.w800),
              )
            ],
          ),
          TextField(
            keyboardType: TextInputType.text,
            controller: _contentController,
            decoration: InputDecoration(hintText: '写下内容'),
            maxLines: 5,
          )
        ],
      ),
    );
  }



  Widget _buildFloatingActionButtion(context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
      width: ScreenUtil().setWidth(750),
      height: ScreenUtil().setHeight(80),
      child: RaisedButton(
        child: Text(
          '回复',
          style: TextStyle(color: Colors.white, fontSize: 18),
        ),
        color: Theme.of(context).primaryColor,
        onPressed: () {
          _submit();
        },
      ),
    );
  }

  _submit() {
    String content = _contentController.text;
    if (content.length < 4) {
      Fluttertoast.showToast(msg: '请输入内容', gravity: ToastGravity.CENTER);
      return;
    }

    var formData = {
      "content": content,
      "forumId": _forumId,
      "userId": Global.profile.user.userId,
      "userNick": Global.profile.user.nick,
      "userAvatar": Global.profile.user.avatar,
      
    };
    HttpUtil.getInstance()
        .post("api/v1/forum/forumReply", formData: formData)
        .then((val) {
      print(val);
      if (val['code'] == '10000') {
        Fluttertoast.showToast(msg: '提交成功', gravity: ToastGravity.CENTER);
        GlobalEventBus.fireRefreshForumDetail();
        Navigator.of(context).pop();
      } else {
        Fluttertoast.showToast(
            msg: val['message'], gravity: ToastGravity.CENTER);
      }
    });
  }

  
}
