import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_family/common/global.dart';
import 'package:my_family/common/global_event.dart';
import 'package:my_family/common/http_util.dart';
import 'package:my_family/common/routers.dart';
import 'package:my_family/models/forum_info.dart';
import 'package:my_family/models/forum_reply.dart';
import 'package:my_family/widgets/dialog.dart';

class ForumDetailPage extends StatefulWidget {
  @override
  _ForumDetailPageState createState() => _ForumDetailPageState();
}

class _ForumDetailPageState extends State<ForumDetailPage> {
  ForumInfo forumInfo;
  List<ForumReply> _replies = new List();

  var _eventSubscription;

  @override
  void initState() {
    super.initState();

    String json = Global.prefs.getString("_forumJson");
    print("json == " + json);
    forumInfo = ForumInfo.fromJson(jsonDecode(json));
    _getForumReplyList();
    _eventSubscription =
        GlobalEventBus().event.on<CommonEventWithType>().listen((event) {
      print("C onEvent:" + event.eventType);
      if (event.eventType == EVENT_REFRESH_FORUM_DETAIL) {
        _getForumReplyList();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _eventSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(forumInfo.title),
        actions: _actions(),
      ),
      body: Container(
        alignment: Alignment(-1, -1),
        margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
        height: ScreenUtil().setHeight(1334),
        width: ScreenUtil().setWidth(750),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              //_buildInputTitle(),
              
              Column(
                children: <Widget>[
                  _buildInputDescription(),
                ],
              ),
              _buildAuthor(),
              _buildReply(),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Global.prefs.setString("_title", forumInfo.title);
          Global.prefs.setString("_forumId", forumInfo.id.toString());
          Routers.router.navigateTo(context, Routers.forumReplyPage);
        },
        backgroundColor: Colors.green,
        tooltip: 'Reply',
        child: Icon(Icons.reply),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  List<Widget> _actions() {
    if (forumInfo.userId == Global.profile.user.userId) {
      return [
        IconButton(
          icon: Icon(Icons.delete),
          onPressed: () {
            showConfirmDialog(context, '确定要删除吗', () {
              _deleteForum();
            });
          },
        )
      ];
    } else {
      return null;
    }
  }

  Widget _buildInputTitle() {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.fromLTRB(10, 0, 10, 20),
      child: Text(
        forumInfo.title,
        style: TextStyle(fontSize: 22),
      ),
    );
  }

  Widget _buildAuthor() {
    var date = new DateTime.fromMillisecondsSinceEpoch(forumInfo.created);
    String updated = " " + formatDate(date, [yyyy, '-' , mm, '-', dd, ' ', HH, ':', nn]);

    return Container(
      alignment: Alignment.topRight,
      padding: EdgeInsets.fromLTRB(10, 10, 10, 20),
      child: Text(
         forumInfo.userNick + ' ' + updated,
        style: TextStyle(fontSize: 12, color: Colors.blueGrey),
      ),
    );
  }

  Widget _buildInputDescription() {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Text(
        forumInfo.content,
        style: TextStyle(fontSize: 14, color: Colors.black87),
      ),
    );
  }

  Widget _buildReply() {
    List<Widget> list = new List();
    for (ForumReply item in _replies) {
      var date = new DateTime.fromMillisecondsSinceEpoch(item.created);
      String updated =
          " " + formatDate(date, [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn]);

      list.add(Divider(
        height: 5,
        indent: 0.0,
        color: Colors.black12,
      ));
      list.add(Container(
        alignment: Alignment.topLeft,
        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Row(
          children: <Widget>[
            Container(
              alignment: Alignment.topLeft,
              height: ScreenUtil().setHeight(72),
              width: ScreenUtil().setWidth(72),
              margin: EdgeInsets.only(right: 5),
              child: _buildHeader(item.userAvatar),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  width: ScreenUtil().setWidth(563),
                  height: ScreenUtil().setHeight(50),
                  margin: EdgeInsets.only(left: 10, bottom: 0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          item.userNick + ' ' + updated,
                          style:
                              TextStyle(fontSize: 12, color: Colors.blueGrey),
                        ),
                      ),
                      item.userId == Global.profile.user.userId
                          ? IconButton(
                              icon: Icon(
                                Icons.delete,
                                size: 14,
                                color: Colors.black54,
                              ),
                              onPressed: () {
                                showConfirmDialog(context, '确定要删除吗', () {
                                  _deleteReply(item.id);
                                });
                              },
                            )
                          : Text('')
                    ],
                  ),
                ),
                Container(
                  width: ScreenUtil().setWidth(563),
                  margin: EdgeInsets.only(left: 10, bottom: 10, top: 0),
                  child: Text(
                    item.content,
                    style: TextStyle(fontSize: 14),
                  ),
                ),
              ],
            )
          ],
        ),
      ));
    }

    return Column(children: list);
  }

  Widget _buildHeader(String avatar) {
    if (avatar != null && avatar.length > 10) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(6),
        child: CachedNetworkImage(
          imageUrl: avatar,
          fit: BoxFit.fill,
          placeholder: (context, url) => CircularProgressIndicator(),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
      );
    }
    {
      return Image.asset('images/dad.png');
    }
  }

  _getForumReplyList() {
    var formDate = {
      "keyword": forumInfo.id.toString(),
      "page": 1,
      "pageSize": 50
    };
    HttpUtil.getInstance()
        .post("api/v1/forum/forumReply/search", formData: formDate)
        .then((val) {
      if (val['code'] == '10000') {
        if (_replies != null) {
          _replies.clear();
        } else {
          _replies = List();
        }

        val['data']['list'].forEach((v) {
          ForumReply info = ForumReply.fromJson(v);
          _replies.add(info);
        });

        setState(() {});
      } else {
        Fluttertoast.showToast(
            msg: val['message'], gravity: ToastGravity.CENTER);
      }
    });
  }

  _deleteForum() {
    HttpUtil.getInstance()
        .delete(
      "api/v1/forum/forumInfo/" + forumInfo.id.toString(),
    )
        .then((val) {
      if (val['code'] == '10000') {
        Fluttertoast.showToast(msg: '删除成功', gravity: ToastGravity.CENTER);
        GlobalEventBus.fireRefreshForumList();
        Navigator.of(context).pop();
      } else {
        Fluttertoast.showToast(
            msg: val['message'], gravity: ToastGravity.CENTER);
      }
    });
  }

  _deleteReply(int id) {
    HttpUtil.getInstance()
        .delete(
      "api/v1/forum/forumReply/" + id.toString(),
    )
        .then((val) {
      if (val['code'] == '10000') {
        Fluttertoast.showToast(msg: '删除成功', gravity: ToastGravity.CENTER);
        _getForumReplyList();
      } else {
        Fluttertoast.showToast(
            msg: val['message'], gravity: ToastGravity.CENTER);
      }
    });
  }
}
