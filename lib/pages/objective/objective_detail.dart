import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_family/common/global.dart';
import 'package:my_family/common/http_util.dart';
import '../../common/utils.dart';

import 'package:shared_preferences/shared_preferences.dart';

class ObjectiveDetailPage extends StatefulWidget {
  final String oid;
  ObjectiveDetailPage(this.oid);
  @override
  _ObjectiveDetailPageState createState() => _ObjectiveDetailPageState(oid);
}

class _ObjectiveDetailPageState extends State<ObjectiveDetailPage> {
  String oid;
  _ObjectiveDetailPageState(this.oid);

  int startTime = 0;
  int endTime = 0;
  DateTime now = DateTime.now();
  Map<String, int> signTimes = Map();

  @override
  void initState() {
    _getDate();
    super.initState();
  }

  _getDate() {
    HttpUtil.getInstance()
        .get(
      "api/v1/obj/objectiveSign/" +
          oid +
          '/' +
          now.millisecondsSinceEpoch.toString(),
    )
        .then((val) {
      print(val);

      setState(() {
        startTime = val['data']['start'];
        endTime = val['data']['end'];
        signTimes.clear();
        val['data']['list'].forEach((v) {
          signTimes[Utils.formatDate2(v['created'])] = 1;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SharedPreferences prefs = Global.prefs;
    String title = prefs.getString("sign_title");
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Column(
        children: <Widget>[
          _buildMonth(),
          Container(
            margin: EdgeInsets.only(top: 20, left: 10, right: 10),
            child: _buildTable(context),
          )
        ],
      ),
      floatingActionButton: _buildFloatingActionButtion(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget _buildMonth() {
    return Container(
      margin: EdgeInsets.only(top:20),
      child: Center(
        child: Text(now.year.toString() + '年' + now.month.toString() + '月', style: TextStyle(fontSize: 22),)
      ),
    );
  }

  FixedColumnWidth _columnWidth(double d) {
    double d = ScreenUtil().setWidth(100).toDouble();
    return FixedColumnWidth(d);
  }

  Widget _buildTable(context) {
    double d = ScreenUtil().setWidth(100).toDouble();
    return Table(
      columnWidths: {
        //列宽
        0: _columnWidth(100),
        1: _columnWidth(100.0),
        2: _columnWidth(100.0),
        3: _columnWidth(100.0),
        4: _columnWidth(100.0),
        5: _columnWidth(100.0),
        6: _columnWidth(100.0),
      },
      //表格边框样式
      border: TableBorder.all(
        color: Colors.black26,
        width: 1.0,
        style: BorderStyle.solid,
      ),
      children: _buildRows(),
    );
  }

  List<TableRow> _buildRows() {

    List<TableRow> rows = List<TableRow> ();
    rows.add(
        TableRow(children: [
          
          _buildTh('周一'),
          _buildTh('周二'),
          _buildTh('周三'),
          _buildTh('周四'),
          _buildTh('周五'),
          _buildTh('周六'),
          _buildTh('周日'),
        ])
      );
    
    int days = ((endTime - startTime)/(1000*60*60*24)).toInt() + 1;
    print('days = ' + days.toString());
    for(int i = 0 ; i < days/7; i++) {
      DateTime today = DateTime.fromMillisecondsSinceEpoch(startTime + i * 1000*60*60*24);
      rows.add(
        TableRow(children: [
          _buildCell((startTime + (i*7 + 0) * 1000*60*60*24).toInt()),
          _buildCell((startTime + (i*7 + 1) * 1000*60*60*24)),
          _buildCell((startTime + (i*7 + 2) * 1000*60*60*24)),
          _buildCell((startTime + (i*7 + 3) * 1000*60*60*24)),
          _buildCell((startTime + (i*7 + 4) * 1000*60*60*24)),
          _buildCell((startTime + (i*7 + 5) * 1000*60*60*24)),
          _buildCell((startTime + (i*7 + 6) * 1000*60*60*24)),
        ])
      );
    }
    return rows;

  }



  Widget _buildTh(String text) {
   

    return SizedBox(
      width: ScreenUtil().setWidth(100),
      height: ScreenUtil().setHeight(100),
      child: Center(
        child: Text(
          text,
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget _buildCell(int datetime) {
     DateTime date = DateTime.fromMillisecondsSinceEpoch(datetime);
    String text = date.day.toString();
    bool sign = signTimes[Utils.formatDate2(datetime)] != null;
    return SizedBox(
      width: ScreenUtil().setWidth(100),
      height: ScreenUtil().setHeight(100),
      child: Center(
        child: Text(
          text,
          style: sign
              ? TextStyle(color: Theme.of(context).accentColor, fontWeight: FontWeight.w900)
              : TextStyle(color: Colors.black12),
        ),
      ),
    );
  }

  Widget _buildFloatingActionButtion(context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
      width: ScreenUtil().setWidth(750),
      height: ScreenUtil().setHeight(80),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          OutlineButton(
            child: Text("<<"),
            onPressed: () {
              int month = now.month;
              now = now.subtract(new Duration(days: 20));
              if(month == now.month) {
                now = now.subtract(new Duration(days: 20));
              }
              _getDate();
            },
          ),
          OutlineButton(
            child: Text(">>"),
            onPressed: () {
              int month = now.month;
              now = now.add(new Duration(days: 20));
              if(month == now.month) {
                now = now.add(new Duration(days: 20));
              }
              _getDate();
            },
          )
        ],
      ),
    );
  }
}
