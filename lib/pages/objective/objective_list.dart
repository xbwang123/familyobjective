import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_family/common/global.dart';
import 'package:my_family/common/global_event.dart';
import 'package:my_family/common/http_util.dart';
import 'package:my_family/common/routers.dart';
import 'package:my_family/models/member.dart';
import 'package:my_family/models/objective.dart';
import 'package:my_family/states/user_model.dart';
import 'package:my_family/widgets/progress.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ObjectiveListPage extends StatefulWidget {
  int userId;
  ObjectiveListPage(this.userId);

  @override
  _ObjectiveListPageState createState() => _ObjectiveListPageState(userId);
}

class _ObjectiveListPageState extends State<ObjectiveListPage> {
  int userId;
  _ObjectiveListPageState(this.userId);

  List<Objective> objectives;
  var _eventSubscription;

  @override
  void initState() {
    super.initState();
    _getObjectiveList();
    _eventSubscription =
        GlobalEventBus().event.on<CommonEventWithType>().listen((event) {
      //print("C onEvent:" + event.eventType);
      print(event.userId.toString() + ' == ' + userId.toString());
      if (event.eventType == EVENT_REFRESH_OBJECTIVELIST) {
        if (event.userId == userId) {
          _refreshObjectiveList();
        }
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _eventSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 10, top: 10, bottom: 0),
      child: RefreshIndicator(
        onRefresh: () => _refreshObjectiveList(),
        child: _buildObjectiveList(),
      ),
    );
  }

  Widget _buildObjectiveList() {
    return ListView.separated(
      itemCount: objectives == null ? 0 : objectives.length,
      itemBuilder: (BuildContext context, int pos) {
        return _buildObjective(objectives[pos]);
      },
      separatorBuilder: (BuildContext context, int index) {
        return Divider(
          color: Colors.black38,
          height: ScreenUtil().setHeight(15),
        );
      },
    );
  }

  Widget _buildObjective(Objective o) {
    return Container(
      //elevation: 1,
      child: Container(
        width: ScreenUtil().setWidth(710),
        height: ScreenUtil().setHeight(150),
        child: Column(
          children: <Widget>[
            _buildObjectiveTitle(o),
            _buildObjectiveProgress(o)
          ],
        ),
      ),
    );
  }

  Widget _buildObjectiveTitle(Objective o) {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(0)),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              o.title,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            ),
          ),
          InkWell(
            child: Container(
              height: ScreenUtil().setHeight(48),
              child: Icon(Icons.calendar_view_day, color: Colors.black45,),
            ),
            onTap: () {
              SharedPreferences prefs = Global.prefs;
              prefs.setString("sign_title", o.title);
              Routers.router.navigateTo(context, Routers.objectiveDetailPage + "?oid=" + o.id.toString());
            },
          )
        ],
      ),
    );
  }

  Widget _buildObjectiveProgress(Objective o) {
    DateTime start = DateTime.fromMillisecondsSinceEpoch(o.startDate);
    DateTime end = DateTime.fromMillisecondsSinceEpoch(o.endDate);
    DateTime now = DateTime.now();
    int actProgress = (o.signTimes * 100 / o.planTimes).toInt();
    int shouldProgress =
        (now.difference(start).inDays * 100 / end.difference(start).inDays)
            .toInt();

    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(15), left: 0, right: 0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: ScreenUtil().setHeight(5)),
                  //width: ScreenUtil().setWidth(500),
                  child: Row(
                    children: <Widget>[
                      Text(
                        '进度 ' +
                            o.signTimes.toString() +
                            '/' +
                            o.planTimes.toString(),
                        style: TextStyle(color: Colors.black45),
                      )
                    ],
                  ),
                ),
                MyProgressWidget(
                    actProgress, shouldProgress, ScreenUtil().setWidth(480), Theme.of(context).accentColor),
              ],
            ),
          ),
          _buildSignButton(o)
        ],
      ),
    );
  }

  Widget _buildSignButton(Objective o) {
    DateTime now = DateTime.now();

    if (o.lastSignTime != null) {
      DateTime sign = DateTime.fromMillisecondsSinceEpoch(o.lastSignTime);
      if (now.day == sign.day &&
          now.year == sign.year &&
          sign.month == now.month) {
        return Container(
          margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
          child: Text(
            '今日已打卡',
            style:
                TextStyle(fontWeight: FontWeight.bold, color: Colors.black45),
          ),
        );
      }
    } else {
      DateTime start = DateTime.fromMillisecondsSinceEpoch(o.startDate);
      print(o.title + " start= " + (start.year*1000 + start.month*32 + start.day).toString() + ", now = " + (now.year*1000 + now.month*32 + now.day).toString());
      if ((start.year*1000 + start.month*32 + start.day) >
          (now.year*1000 + now.month*32 + now.day)) {
        return Container(
          margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
          child: Text(
            '还未开始',
            style:
                TextStyle(fontWeight: FontWeight.bold, color: Colors.black45),
          ),
        );
      }
    }

    return Container(
      margin: EdgeInsets.only(top:5),
      height: ScreenUtil().setHeight(55),
      width: ScreenUtil().setWidth(150),
      child: FlatButton.icon(
      icon: Icon(
        Icons.done,
        color: Colors.white,
        size: 12,
      ),
      color: Theme.of(context).accentColor,
      label: Text(
        '打卡',
        style: TextStyle(color: Colors.white, fontSize: 12),
      ),
      onPressed: () {
        SharedPreferences prefs = Global.prefs;
        prefs.setInt("sign_objectiveId", o.id);
        prefs.setString("sign_title", o.title);
        Routers.router.navigateTo(
          context,
          Routers.objectiveSignPage,
        );
      },
     
    ),
    );
  }

  _getObjectiveList() async {
    List<Objective> familyObjectives =
        Provider.of<UserModel>(context, listen: false).objectives;
    if (familyObjectives != null && familyObjectives.length > 0) {
      setState(() {
        objectives = Global.profile.getUserObjecttiveList(userId);
      });
      return;
    } else {
      _refreshObjectiveList();
    }
  }



  Future<Null> _refreshObjectiveList() async {
    await HttpUtil.getInstance()
        .get(
      "api/v1/obj/objective/unfinished/" +
          Global.profile.user.familyId.toString(),
    )
        .then((val) {
      if (val['code'] == '10000') {
        if (objectives != null) {
          objectives.clear();
        } else {
          objectives = List();
        }
        List<Objective> list = new List<Objective>();
        val['data'].forEach((v) {
          Objective member = Objective.fromJson(v);
          list.add(member);
        });
        Provider.of<UserModel>(context, listen: false).objectives = list;
        setState(() {
          objectives = Global.profile.getUserObjecttiveList(userId);
        });
      } else {
        Fluttertoast.showToast(
            msg: val['message'], gravity: ToastGravity.CENTER);
      }
    });
  }
}
