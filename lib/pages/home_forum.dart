import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:my_family/common/global.dart';
import 'package:my_family/common/global_event.dart';
import 'package:my_family/common/http_util.dart';

import 'package:my_family/common/routers.dart';
import 'package:my_family/common/utils.dart';
import 'package:my_family/models/forum_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeForumPage extends StatefulWidget {
  @override
  _HomeForumState createState() => _HomeForumState();
}

class _HomeForumState extends State<HomeForumPage>
    with SingleTickerProviderStateMixin {
  List<ForumInfo> forumList = List();
  var _eventSubscription;

  @override
  void initState() {
    super.initState();
    _getForumList();
    _eventSubscription =
        GlobalEventBus().event.on<CommonEventWithType>().listen((event) {
      print("C onEvent:" + event.eventType);
      if (event.eventType == EVENT_REFRESH_FORUM_LIST) {
        _getForumList();
      }
    });

  }

  @override
  void dispose() {
    super.dispose();
    _eventSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('交流反馈'),
        actions: <Widget>[
          FlatButton.icon(
            icon: Icon(Icons.add),
            label: Text(''),
            onPressed: () {
              Routers.router.navigateTo(context, Routers.forumNewPage);
            },
          )
        ],
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    if (forumList != null) {
      return _buildForumList();
    } else {
      return Center(
        child: Text('正在加载...'),
      );
    }
  }

  Widget _buildForumList() {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
      height: ScreenUtil().setHeight(1334),
      width: ScreenUtil().setWidth(750),
      child: ListView.separated(
        itemCount: forumList.length,
        itemBuilder: (BuildContext context, int pos) {
          return _buildForumItem(forumList[pos], context);
        },
        separatorBuilder: (BuildContext context, int pos) {
          return Divider(height: 1,);
        },
      ),
    );
  }

  Widget _buildForumItem(ForumInfo item, context) {

    String updated = " " + Utils.formatDate3(item.created);
    return ListTile(

      title: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              item.userNick ,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: Colors.blueGrey, fontSize: 12),
            ),
          ),
          Text(
            updated,
            style: TextStyle(color: Colors.black45,fontSize: 12),
          )
        ],
      ),
      leading: Container(

        height: ScreenUtil().setHeight(64),
        child: _buildHeader(item.userAvatar),
      ),
      subtitle: Text(item.title, maxLines: 1, overflow: TextOverflow.ellipsis)
          ,
      trailing: Icon(Icons.keyboard_arrow_right),
      isThreeLine: true,
      onTap: () {
        SharedPreferences prefs = Global.prefs;
        print(jsonEncode(item.toJson()));
        prefs.setString("_forumJson", jsonEncode(item.toJson()));
        //prefs.setInt("customerId", item.customerId);
        Routers.router.navigateTo(context, Routers.forumDetailPage);
      },
    );
  }

  Widget _buildHeader(String avatar) {
    if (avatar != null && avatar.length > 10) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(6),
        child: CachedNetworkImage(
          imageUrl: avatar,
          fit: BoxFit.fill,
          placeholder: (context, url) => CircularProgressIndicator(),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
      );
    }  {
      return Image.asset('images/dad.png');
    }
  }

  _getForumList() {
    var formDate = {"keyword": "", "page": 1, "pageSize": 50};
    HttpUtil.getInstance()
        .post(
      "api/v1/forum/forumInfo/search" , formData: formDate
    )
        .then((val) {
      if (val['code'] == '10000') {
        if (forumList != null) {
          forumList.clear();
        } else {
          forumList = List();
        }

        val['data']['list'].forEach((v) {
          ForumInfo info = ForumInfo.fromJson(v);
          forumList.add(info);
        });

        setState(() {
          
        });
      } else {
        Fluttertoast.showToast(
            msg: val['message'], gravity: ToastGravity.CENTER);
      }
    });
  }
}
