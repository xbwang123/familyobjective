import 'dart:async';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DownloadPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Completer<WebViewController> _controller =
      Completer<WebViewController>();

    return Scaffold(
      appBar: AppBar(title: Text('软件说明'),),
      body: Builder(builder: (BuildContext context) {
        return WebView(
          initialUrl: 'http://www.shellsports.cn/readme.html',
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
    
          navigationDelegate: (NavigationRequest request) {
        
            print('allowing navigation to $request');
            return NavigationDecision.navigate;
          },
          onPageStarted: (String url) {
            print('Page started loading: $url');
          },
          onPageFinished: (String url) {
            print('Page finished loading: $url');
          },
          gestureNavigationEnabled: true,
        );
      }),
    );
  }
}