import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:my_family/common/cloud_api.dart';

import 'package:my_family/common/global_event.dart';

import 'package:my_family/common/routers.dart';
import 'package:my_family/common/utils.dart';
import 'package:my_family/models/member.dart';

import 'package:my_family/states/user_model.dart';
import 'package:my_family/widgets/dialog.dart';
import 'package:provider/provider.dart';

class MemberSettingPage extends StatefulWidget {
  @override
  _MemberSettingPageState createState() => _MemberSettingPageState();
}

class _MemberSettingPageState extends State<MemberSettingPage> {
  var _eventSubscription;
  List<Member> members = List();

  @override
  void initState() {
    //_getListMember();
    members = Provider.of<UserModel>(context, listen: false).members;
    _eventSubscription =
        GlobalEventBus().event.on<CommonEventWithType>().listen((event) {
      print("C onEvent:" + event.eventType);
      if (event.eventType == EVENT_MEMBER_CHANGED) {
        _getListMember();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _eventSubscription.cancel();
  }

  void _getListMember() {
    getListMember(context, (){
      setState(() {
          members = Provider.of<UserModel>(context, listen: false).members;
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('家庭成员管理'),
        actions: <Widget>[
          FlatButton.icon(
            onPressed: () async {
              Routers.router
                  .navigateTo(context, Routers.addMemberPage, replace: false);
            },
            //backgroundColor: Colors.green,
            label: Text(''),
            icon: Icon(
              Icons.add,
              //color: Theme.of(context).accentColor,
            ),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              width: ScreenUtil().setWidth(750),
              child: _homeMemberList(),
            ),
          ),
        ],
      ),
    );
  }




  Widget _homeMemberList() {
    return ListView.separated(
      itemCount: members.length,
      itemBuilder: (BuildContext context, int pos) {
        return ListTile(
          leading: Container(
            child: Utils.getMemberHeader(members[pos]),
            height: ScreenUtil().setHeight(64),
          ),
          title: Text(members[pos].nick),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Routers.router.navigateTo(
                context,
                Routers.addMemberPage +
                    "?memberId=" +
                    members[pos].userId.toString(),
                replace: false);
          },
        );
      },
      separatorBuilder: (BuildContext context, int pos) {
        return Divider(height: 0,);
      },
    );
  }


}
