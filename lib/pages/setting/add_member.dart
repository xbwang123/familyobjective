import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_family/common/cloud_api.dart';
import 'package:my_family/common/global.dart';
import 'package:my_family/common/global_event.dart';
import 'package:my_family/common/http_util.dart';
import 'package:my_family/common/utils.dart';
import 'package:my_family/models/member.dart';
import 'package:my_family/models/user.dart';
import 'package:my_family/widgets/dialog.dart';
import 'package:my_family/widgets/input.dart';
import 'package:my_family/widgets/loading_dlg.dart';

class AddMemberPage extends StatefulWidget {
  final String memberId;
  AddMemberPage(this.memberId);
  @override
  _AddMemberPageState createState() => _AddMemberPageState(memberId);
}

class _AddMemberPageState extends State<AddMemberPage> {
  final String memberId;
  _AddMemberPageState(this.memberId);

  final TextEditingController _roleController =
      TextEditingController.fromValue(TextEditingValue(text: ''));
  final TextEditingController _nickController =
      TextEditingController.fromValue(TextEditingValue(text: ''));
  final TextEditingController _loginNameController =
      TextEditingController.fromValue(TextEditingValue(text: ''));
  final TextEditingController _passwordController =
      TextEditingController.fromValue(TextEditingValue(text: ''));

  bool readonly = false;
  final List<String> roles = List();
  var _image = null;

  @override
  Widget build(BuildContext context) {
    roles.clear();
    roles.addAll(Utils.rolenames());
    if (memberId != '0' && _roleController.text == '') {
      List<Member> members = Global.profile.members;
      for (Member m in members) {
        if (m.userId == int.parse(memberId)) {
          _roleController.value =
              TextEditingValue(text: Utils.roleToRolename(m.familyRole));
          _nickController.value = TextEditingValue(text: m.nick);

          if (m.loginName != null && m.loginName.length > 0) {
            _loginNameController.value = TextEditingValue(text: m.loginName);
            readonly = true;
          }
          //_passwordController.value = TextEditingValue(text: m.password);
        }
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(memberId == '0' ? '添加家庭成员' : '修改家庭成员'),
        actions: <Widget>[
          _buildActionButtion(context),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _buildAvatar(),
            _buildInputFamilyRole(context),
            _buildInputNick(),
            _buildInputLoginName(),
            _buildInputPassword(),
            Container(
              width: ScreenUtil().setWidth(710),
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Text(
                '',
                style: TextStyle(color: Colors.black45),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: _buildFloatingActionButtion(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }

  Widget _buildFloatingActionButtion(context) {
    //Member member = Global.profile.getMember(int.parse(memberId));
    User user = Global.profile.user;
    if (memberId == '0' ||
        memberId == user.userId.toString() ||
        user.familyRole == 'SON' ||
        user.familyRole == 'DAUGHTER') {
      return Container();
    }
    return FlatButton.icon(
      onPressed: () async {
        showConfirmDialog(context, '确认要删除吗', () {
          HttpUtil.getInstance()
              .delete(
            "/api/v1/ums/user/" + memberId,
          )
              .then((val) {
            print(val);
            if (val['code'] == '10000') {
              GlobalEventBus.fireMemberChanged();
              Navigator.pop(context);
            } else {
              Fluttertoast.showToast(
                  msg: val['message'], gravity: ToastGravity.CENTER);
            }
          });
        });
      },
      //backgroundColor: Colors.green,
      label: Text('删除'),
      icon: Icon(
        Icons.delete,
      ),
    );
  }

  Widget _buildActionButtion(context) {
    return FlatButton.icon(
      icon: Icon(Icons.save),
      label: Text('保存'),
      onPressed: () {
        String nick = _nickController.text;

        if (nick.length < 1) {
          Fluttertoast.showToast(msg: '请输入昵称', gravity: ToastGravity.CENTER);
          return;
        }
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return new LoadingDialog(
                text: "正在保存...",
              );
            });

        String familyRole = Utils.rolenameToRole(_roleController.text);

        FormData formData = new FormData.fromMap({
          "familyRole": familyRole,
          "userId": Global.profile.user.userId,
          "memberId": memberId,
          "nick": nick,
          "avatar": "",
          "familyId": Global.profile.user.familyId,
          "loginName": _loginNameController.text,
          "password": _passwordController.text,
        });
        String url = "/api/v1/ums/user/updateMember/";
        if (_image != null) {
          formData.files.add(MapEntry(
            "file",
            MultipartFile.fromFileSync(_image.path, filename: "aa.png"),
          ));

          url += "withHeader";
        }

        print(formData);
        HttpUtil.getInstance().post(url, formData: formData).then((val) {
          Navigator.pop(context);
          print(val);
          if (val['code'] == '10000') {
            GlobalEventBus.fireMemberChanged();
            Fluttertoast.showToast(msg: '保存成功', gravity: ToastGravity.CENTER);
            Navigator.pop(context);
          } else {
            Fluttertoast.showToast(
                msg: val['message'], gravity: ToastGravity.CENTER);
          }
        });
        //
      },
    );
  }

  Widget _buildInputFamilyRole(context) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
      child: Stack(
        alignment: Alignment(1, 1),
        children: <Widget>[
          buildInputWithTitle(_roleController, '成员角色', '选择角色', false, () {
            FocusScope.of(context).requestFocus(new FocusNode());

            Picker picker = new Picker(
                adapter: PickerDataAdapter<String>(pickerdata: roles),
                changeToFirst: true,
                cancelText: '取消',
                confirmText: '确定',
                height: ScreenUtil().setHeight(350),
                textAlign: TextAlign.left,
                columnPadding: const EdgeInsets.all(8.0),
                onConfirm: (Picker picker, List value) {
                  var v = value[0];
                  _roleController.value = TextEditingValue(text: roles[v]);

                  print(_roleController.value);
                });
            picker.showModal(context);
          }),
          IconButton(
            icon: Icon(
              Icons.keyboard_arrow_down,
              color: Colors.black26,
            ),
            onPressed: () {},
          ),
        ],
      ),
    );
  }

  Widget _buildAvatar() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
      height: ScreenUtil().setHeight(156),
      child: Stack(
        alignment: Alignment(0, 1.7),
        children: <Widget>[
          Center(
            child: InkWell(
              child: _buildImage(),
              onTap: () {
                _selectImage();
              },
            ),
          ),
          Text(
            '选择头像',
            style: TextStyle(color: Colors.black45),
          )
        ],
      ),
    );
  }

  Widget _buildImage() {
    Member member = Global.profile.getMember(int.parse(memberId));
    if (_image != null) {
      return ClipRRect(
        child: Image.file(_image),
        borderRadius: BorderRadius.circular(6),
      );
    } else {
      return member.hasAvatar()
          ? ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: CachedNetworkImage(
                imageUrl: member.avatar,
                fit: BoxFit.fill,
                placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            )
          : Icon(
              Icons.account_box,
              size: ScreenUtil().setWidth(156),
            );
    }
  }

  Future _selectImage() async {
    var imageFile = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 480, maxHeight: 720);
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: imageFile.path,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
                //CropAspectRatioPreset.ratio3x2,
                //CropAspectRatioPreset.original,
                //CropAspectRatioPreset.ratio4x3,
                //CropAspectRatioPreset.ratio16x9
              ]
            : [
                //CropAspectRatioPreset.original,
                CropAspectRatioPreset.square,
                /*CropAspectRatioPreset.ratio3x2,
              CropAspectRatioPreset.ratio4x3,
              CropAspectRatioPreset.ratio5x3,
              CropAspectRatioPreset.ratio5x4,
              CropAspectRatioPreset.ratio7x5,
              CropAspectRatioPreset.ratio16x9*/
              ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: '剪切头像',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.square,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
            title: '剪切头像',
            minimumAspectRatio: 1.0,
            rectX: 0.0,
            rectY: 0.0,
            rectWidth: 320.0,
            rectHeight: 320.0,
            cancelButtonTitle: '取消',
            doneButtonTitle: '确定'));
    if (croppedFile != null) {
      setState(() {
        _image = croppedFile;
      });
    }
  }

  Widget _buildInputNick() {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
      child: Stack(
        alignment: Alignment(1, 1),
        children: <Widget>[
          buildInputWithTitle(_nickController, '成员昵称', '家庭称谓', false, () {}),
          //buildClearButton(_nickController),
        ],
      ),
    );
  }

  Widget _buildInputLoginName() {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
      child: Stack(
        alignment: Alignment(1, 1),
        children: <Widget>[
          buildInputWithTitle(
              _loginNameController, '登录账号', '请输入登录账号', readonly, () {}),
          //buildClearButton(_loginNameController),
        ],
      ),
    );
  }

  Widget _buildInputPassword() {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
      child: Stack(
        alignment: Alignment(1, 1),
        children: <Widget>[
          buildInputWithTitle(_passwordController, '登录密码',
              readonly ? '不输入则不修改原有密码' : '请输入登录密码', false, () {}),
          //buildClearButton(_passwordController),
        ],
      ),
    );
  }
}
