import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_family/common/cloud_api.dart';
import 'package:my_family/common/global.dart';
import 'package:my_family/common/global_event.dart';
import 'package:my_family/common/routers.dart';
import 'package:my_family/common/utils.dart';
import 'package:my_family/models/member.dart';
import 'package:my_family/models/objective.dart';
import 'package:my_family/states/user_model.dart';
import 'package:provider/provider.dart';

class FamilyPage extends StatefulWidget {
  @override
  _FamilyPageState createState() => _FamilyPageState();
}

class _FamilyPageState extends State<FamilyPage> {
  var _eventSubscription;
  List<Member> members;


  @override
  void initState() {
    members = Global.profile.members;
    members.sort((left, right) => right.coinsTotal.compareTo(left.coinsTotal));
    _getListMember();
    _eventSubscription =
        GlobalEventBus().event.on<CommonEventWithType>().listen((event) {
      print("C onEvent:" + event.eventType);
      if (event.eventType == EVENT_MEMBER_CHANGED) {
        _getListMember();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _eventSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('家庭成员'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _buildCoins(),
          ],
        ),
      ),
    );
  }

  Widget _buildCoins() {
    List<Widget> children = List();
    children.add(new Container(
      margin: EdgeInsets.only(top: 10),
    ));
    int i = 0;
    for (Member member in members) {
      children.add(_buildMemberDetail(i, member));
      i++;
    }
    return Column(
      children: children,
    );
  }

  Widget _buildMemberDetail(int idx, Member member) {
    List<Objective> list = Global.profile.getUserObjecttiveList(member.userId);
    int total = 0;
    int totalProgress = 0;
    int totalSign = 0;
    int planSign = 0;
    DateTime now = DateTime.now();
    String strProgress = "";
    for(Objective o in list) {
      DateTime start = DateTime.fromMillisecondsSinceEpoch(o.startDate);
      DateTime end = DateTime.fromMillisecondsSinceEpoch(o.endDate);
      
      totalSign += o.signTimes;

      planSign +=
          (now.difference(start).inDays * o.planTimes / end.difference(start).inDays)
              .toInt();
     
      print(member.nick + " = " + totalSign.toString() + "/" + planSign.toString() + " days=" + now.difference(start).inDays.toString() + "/" + end.difference(start).inDays.toString());
      total++;
    }
    Color titleColor = Colors.black;
    if(planSign > 0) {
      totalProgress = (totalSign *100/planSign).toInt();
    }
    
    if(total < 3) {
      strProgress = "缺少目标";
      titleColor = Colors.red[400];
    }
    else if(totalProgress >= 100) {
      strProgress = "锲而不舍";
      titleColor = Colors.green[800];
    } else if(totalProgress >= 80) {
      strProgress = "再接再厉";
      titleColor = Colors.green[400] ;
    } else if(totalProgress > 60) {
      strProgress = "漫不经心";
      titleColor = Colors.black45;
    } else if(totalProgress > 40) {
      strProgress = "吊儿郎当";
      titleColor = Colors.yellow[700];
    } else {
      strProgress = "浑浑噩噩";
      titleColor = Colors.red[900];
    }

    return Container(
      margin: EdgeInsets.only(left: 10, right: 10, top: 2),
      child: new Card(
        elevation: 2.0, //设置阴影
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0))), //设置圆角
        child: new Column(
          // card只能有一个widget，但这个widget内容可以包含其他的widget
          children: [
            new ListTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(strProgress + ' - ' + member.nick , style: new TextStyle(color: titleColor)),
                  idx == 0 ? Image.asset('images/gold.png', width: ScreenUtil().setWidth(32),) : Text('')
                ],
              ),
              trailing: Icon(Icons.keyboard_arrow_right),
              leading: Container(
                height: ScreenUtil().setHeight(80),
                child: Stack(
                  alignment: Alignment.topLeft,
                  children: <Widget>[
                    Utils.getMemberHeader(member),
              
                  ],
                ),
              ),
              onTap: () {
                Routers.router.navigateTo(
                    context,
                    Routers.memberHomePage +
                        "?memberId=" +
                        member.userId.toString(),
                    replace: false);
              },
            ),
            new Divider(
              height: 0,
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              height: ScreenUtil().setHeight(110),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Image.asset(
                        'images/coin.png',
                        color: Theme.of(context).accentColor,
                        width: ScreenUtil().setWidth(28),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text('金币: ' +
                            (member.coinsTotal - member.coinsUsed).toString(), style: TextStyle(color: Colors.black45, fontSize: 12),),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[Image.asset(
                        'images/objective.png',
                        color: Theme.of(context).accentColor,
                        width: ScreenUtil().setWidth(32),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text('目标:' +total.toString()+'个', style: TextStyle(color: Colors.black45, fontSize: 12),),
                      )],
                  ),
                  Column(
                    children: <Widget>[
                      Image.asset(
                        'images/hard.png',
                        color: Theme.of(context).accentColor,
                        width: ScreenUtil().setWidth(32),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text('毅力: '+totalProgress.toString()+'%', style: TextStyle(color: Colors.black45, fontSize: 12),),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _getListMember() {
    getListMember(context, () {
      setState(() {
        members = Provider.of<UserModel>(context, listen: false).members;
        members
            .sort((left, right) => right.coinsTotal.compareTo(left.coinsTotal));
      });
    });
  }
}
