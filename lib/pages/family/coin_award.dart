import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_family/common/global.dart';
import 'package:my_family/common/global_event.dart';
import 'package:my_family/common/http_util.dart';
import 'package:my_family/models/member.dart';


import 'package:shared_preferences/shared_preferences.dart';

class CoinAwardPage extends StatefulWidget {
  final String memberId;
  CoinAwardPage(this.memberId);
  @override
  _CoinAwardPageState createState() => _CoinAwardPageState(memberId);
}

class _CoinAwardPageState extends State<CoinAwardPage> {

  final String memberId;
  _CoinAwardPageState(this.memberId);

  final TextEditingController _titleController =
      TextEditingController.fromValue(TextEditingValue(text: ''));

  final TextEditingController _coinsController =
      TextEditingController.fromValue(TextEditingValue(text: ''));

  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      appBar: AppBar(
        title: Text('奖励金币'),
      ),
      body: Column(
        children: <Widget>[
          _buildInputTitle(),
          _buildInputCoins(context),
        ],
      ),
      floatingActionButton: _buildFloatingActionButtion(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }


  Widget _buildInputTitle() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(Icons.insert_comment, color: Theme.of(context).accentColor),
              Text(
                ' 奖励原因',
                style: TextStyle(fontWeight: FontWeight.w800),
              )
            ],
          ),
          TextField(
            keyboardType: TextInputType.text,
            controller: _titleController,
            decoration: InputDecoration(hintText: '例如：小朋友帮忙做家务'),
            maxLines: 1,
          )
        ],
      ),
    );
  }

  Widget _buildInputCoins(context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(Icons.monetization_on, color: Theme.of(context).accentColor),
              Text(
                ' 金币数量',
                style: TextStyle(fontWeight: FontWeight.w800),
              )
            ],
          ),
          TextField(
            keyboardType: TextInputType.number,
            controller: _coinsController,
            decoration: InputDecoration(hintText: '奖励的金币数量'),
            maxLines: 1,
          )
        ],
      ),
    );
  }

  Widget _buildFloatingActionButtion(context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
      width: ScreenUtil().setWidth(750),
      height: ScreenUtil().setHeight(80),
      child: RaisedButton(
        child: Text(
          '给予奖励',
          style: TextStyle(color: Colors.white, fontSize: 18),
        ),
        color: Theme.of(context).primaryColor,
        onPressed: () {

  
          String title = _titleController.text;
          if(title.length < 2) {
            Fluttertoast.showToast(
                  msg:'请输入奖励原因', gravity: ToastGravity.CENTER);
            return ;
          }

          String coins = _coinsController.text;
          if(coins.length < 1 || int.parse(coins) < 1) {
            Fluttertoast.showToast(
                  msg:'请输入奖励的金币个数', gravity: ToastGravity.CENTER);
            return ;
          }

          var formData = {
            "changeType": 1,
            "coins": coins,

            "familyId": Global.profile.user.familyId,
            "reason": title,
            "userId": memberId
          };
          HttpUtil.getInstance()
              .post("api/v1/ums/coinsChange/award", formData: formData)
              .then((val) {
            print(val);
            if (val['code'] == '10000') {
              GlobalEventBus.fireMemberChanged();
              Navigator.of(context).pop();
            } else {
              Fluttertoast.showToast(
                  msg: val['message'], gravity: ToastGravity.CENTER);
            }
          });
        },
      ),
    );
  }
}
