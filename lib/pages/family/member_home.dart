import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_family/common/global.dart';
import 'package:my_family/common/http_util.dart';
import 'package:my_family/common/routers.dart';
import 'package:my_family/common/utils.dart';
import 'package:my_family/models/member.dart';
import 'package:my_family/models/objective.dart';
import 'package:my_family/widgets/list_memu_item.dart';
import 'package:my_family/widgets/progress.dart';

class MemberHomePage extends StatelessWidget {
  final String memberId;
  MemberHomePage(this.memberId);

  @override
  Widget build(BuildContext context) {
    Member member = Global.profile.getMember(int.parse(memberId));
    print('memberId = ' + memberId);
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            _topHeader(context, member),
            _coninAward(context),
            _coninExchange(context),
            _coinsChangeColor(context),
            Container(
              width: ScreenUtil().setWidth(730),
              margin: EdgeInsets.only(top: 10, left: 10, right: 10),
              child: Text('历史目标',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
            ),
            _buildFinishedObjectives()
          ],
        ),
      ),
      floatingActionButton: Container(
        margin: EdgeInsets.only(top: 100),
        child: IconButton(
            icon: Icon(
              Icons.keyboard_arrow_left,
              color: Colors.white,
              size: 48,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
    );
  }

  Widget _topHeader(context, Member member) {
    return Container(
      width: ScreenUtil().setWidth(750),
      height: ScreenUtil().setHeight(450),
      padding: EdgeInsets.all(0),
      color: Theme.of(context).accentColor,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          //_picture(),
          ClipOval(
            child: Container(
              height: ScreenUtil().setHeight(156),
              child: Utils.getMemberHeader(member),
            ),
          ),
          Positioned(
            bottom: 20,
            width: ScreenUtil().setWidth(750),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _buildCoins('总数:' + member.coinsTotal.toString()),
                Text(member.nick),
                _buildCoins(
                    '剩余:' + (member.coinsTotal - member.coinsUsed).toString())
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildCoins(String title) {
    return Container(
      height: ScreenUtil().setHeight(110),
      child: Column(
        children: <Widget>[
          Image.asset(
            "images/coin.png",
            height: ScreenUtil().setHeight(32),
          ),
          Container(
            margin: EdgeInsets.only(top: 5),
            child: Text(title,
                style: TextStyle(
                  color: Colors.white,
                )),
          )
        ],
      ),
    );
  }

  Widget _coinsChangeColor(context) {
    return buildListMenuItem(context, Icons.list, '金币变化', () {
      Routers.router.navigateTo(
          context, Routers.coinChangeListPage + "?memberId=" + memberId,
          replace: false);
    });
  }

  Widget _buildFinishedObjectives() {
    return FutureBuilder(
      future:
          _getFinishedObjective(), // 用户定义的需要异步执行的代码，类型为Future<String>或者null的变量或函数
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        //snapshot就是_calculation在时间轴上执行过程的状态快照
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Text('加载中...'); //如果_calculation正在执行则提示：加载中
          default: //如果_calculation执行完毕
            if (snapshot.hasError) //若_calculation执行出现异常
              return Text('错误: ${snapshot.error}');
            else //若_calculation执行正常完成
              return _createListView(context, snapshot);
        }
      },
    );
  }

  Widget _createListView(BuildContext context, AsyncSnapshot snapshot) {
    print(snapshot.data['data']);
    var objectives = snapshot.data['data'];
    return Expanded(
      child: ListView.separated(
        itemCount: objectives == null ? 0 : objectives.length,
        itemBuilder: (BuildContext context, int pos) {
          return _buildObjective(Objective.fromJson(objectives[pos]));
        },
        separatorBuilder: (BuildContext context, int index) {
          return Container(
            margin: EdgeInsets.only(left: 10, right: 10),
            child: Divider(
              color: Colors.black12,
              height: 10,
            ),
          );
        },
      ),
    );
  }

  Widget _buildObjective(Objective o) {
    return Container(
      //elevation: 1,
      margin: EdgeInsets.only(left: 10, right: 10),
      child: Container(
        width: ScreenUtil().setWidth(710),
        height: 60,
        child: Column(
          children: <Widget>[
            _buildObjectiveTitle(o),
            _buildObjectiveProgress(o)
          ],
        ),
      ),
    );
  }

  Widget _buildObjectiveTitle(Objective o) {
    int finished = (o.signTimes * 100 / o.planTimes).toInt();
    return Container(
      margin: EdgeInsets.only(top: 5),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              o.title,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 15),
            ),
          ),
          Text(
            '完成度 ' + finished.toString() + '%',
            style: TextStyle(
                color: finished > 95
                    ? Colors.green
                    : (finished > 70 ? Colors.orange : Colors.red)),
          )
        ],
      ),
    );
  }

  Widget _buildObjectiveProgress(Objective o) {
    DateTime start = DateTime.fromMillisecondsSinceEpoch(o.startDate);
    DateTime end = DateTime.fromMillisecondsSinceEpoch(o.endDate);
    DateTime now = DateTime.now();
    int actProgress = (o.signTimes * 100 / o.planTimes).toInt();
    int shouldProgress =
        (now.difference(start).inDays * 100 / end.difference(start).inDays)
            .toInt();

    return Container(
      margin: EdgeInsets.only(top: 0, left: 0, right: 0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: 10),
              child: MyProgressWidget(
                actProgress,
                0,
                ScreenUtil().setWidth(730),
                Colors.blue,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _coninExchange(context) {
    return Container(
      margin: EdgeInsets.only(top: 0),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(bottom: BorderSide(width: 1, color: Colors.black12))),
      child: ListTile(
        leading: Image.asset("images/minus.png",
            width: ScreenUtil().setWidth(48),
            color: Theme.of(context).accentColor),
        title: Text('金币兑换'),
        trailing: Icon(Icons.keyboard_arrow_right),
        onTap: () {
          if (Global.isAdult()) {
            Routers.router.navigateTo(
                context, Routers.coinExchangePage + "?memberId=" + memberId,
                replace: false);
          } else {
            Fluttertoast.showToast(
                msg: '小朋友不要乱点', gravity: ToastGravity.CENTER);
            return;
          }
        },
      ),
    );
  }

  Widget _coninAward(context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(bottom: BorderSide(width: 1, color: Colors.black12))),
      child: ListTile(
        leading: Image.asset("images/add.png",
            width: ScreenUtil().setWidth(48),
            color: Theme.of(context).accentColor),
        title: Text('来点奖励'),
        trailing: Icon(Icons.keyboard_arrow_right),
        onTap: () {
          
          if (Global.isAdult()) {
            Routers.router.navigateTo(
              context, Routers.coinAwardPage + "?memberId=" + memberId,
              replace: false);
          } else {
            Fluttertoast.showToast(
                msg: '小朋友不要乱点', gravity: ToastGravity.CENTER);
            return;
          }
        },
      ),
    );
  }

  Future _getFinishedObjective() async {
    return HttpUtil.getInstance()
        .get("api/v1/obj/objective/finished/" + memberId);
  }
}
