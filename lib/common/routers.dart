import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import 'package:my_family/pages/family/coin_award.dart';
import 'package:my_family/pages/family/coin_change_list.dart';
import 'package:my_family/pages/family/coin_exchange.dart';
import 'package:my_family/pages/family/member_home.dart';
import 'package:my_family/pages/forum/forum_detail.dart';
import 'package:my_family/pages/forum/forum_new.dart';
import 'package:my_family/pages/forum/forum_reply.dart';
import 'package:my_family/pages/home.dart';
import 'package:my_family/pages/login.dart';
import 'package:my_family/pages/objective/objective_detail.dart';
import 'package:my_family/pages/objective/objective_new.dart';
import 'package:my_family/pages/objective/objective_sign.dart';
import 'package:my_family/pages/setting/add_member.dart';
import 'package:my_family/pages/setting/download.dart';
import 'package:my_family/pages/setting/init_setting.dart';
import 'package:my_family/pages/setting/member_setting.dart';
import 'package:my_family/pages/setting/theme_setting.dart';



class Routers {
  static Router router;

  static String root = '/';
  static String loginPage = '/login';
  static String homePage = '/homePage';
  static String initSettingPage = '/initSettingPage';
  static String themeSettingPage = '/themeSettingPage';
  static String memberSettingPage = '/memberSettingPage';
  static String addMemberPage = '/addMemberPage';
  static String newObjectivePage = '/newObjectivePage';
  static String objectiveSignPage = '/objectiveSignPage';
  static String coinExchangePage = '/coinExchangePage';
  static String coinChangeListPage = '/coinChangeListPage';
  static String coinAwardPage = '/coinAwardPage';
  static String objectiveDetailPage = '/objectiveDetailPage';
  static String memberHomePage = '/memberHomePage';
  static String forumNewPage = '/forumNewPage';
  static String forumDetailPage = '/forumDetailPage';
  static String forumReplyPage = '/forumReplyPage';
  static String downloadPage = '/downloadPage';


 
  static void configRoutes(Router router) {
    Routers.router = router;
    router.notFoundHandler = new Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
          return Text('not found');
    });
    router.define(loginPage, handler: _buildHandler(LoginPage()));
    router.define(homePage, handler: _buildHandler(HomePage()));
    router.define(initSettingPage, handler:_buildHandler(InitSettingPage()));
    router.define(themeSettingPage, handler:_buildHandler(ThemeSettingPage()));
    router.define(newObjectivePage, handler:_buildHandler(NewObjectivePage()));
    router.define(objectiveSignPage, handler:_buildHandler(ObjectiveSignPage()));

    router.define(memberSettingPage, handler:_buildHandler(MemberSettingPage()));
    router.define(forumNewPage, handler:_buildHandler(ForumNewPage()));
    router.define(forumDetailPage, handler:_buildHandler(ForumDetailPage()));
    router.define(forumReplyPage, handler:_buildHandler(ForumReplyPage()));
    router.define(downloadPage, handler:_buildHandler(DownloadPage()));

    
    
    router.define(coinExchangePage, handler:Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return CoinExchangePage(params['memberId'].first);
    }));

    router.define(coinAwardPage, handler:Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return CoinAwardPage(params['memberId'].first);
    }));

    router.define(memberHomePage, handler:Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return MemberHomePage(params['memberId'].first);
    }));

    
    router.define(coinChangeListPage, handler:Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return CoinChangeListPage(params['memberId'].first);
    }));
    router.define(objectiveDetailPage, handler:Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return ObjectiveDetailPage(params['oid'].first);
    }));
    

    router.define(addMemberPage, handler:Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      String memberId = '0';
      if (params['memberId'] != null) {
        memberId = params['memberId'].first;
      } 
      print("memberId = " + memberId);
      return AddMemberPage(memberId);
    }));
  }

  static Handler _buildHandler(Widget widget) {
    return Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return widget;
    });
  }

}
