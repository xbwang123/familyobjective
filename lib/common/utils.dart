import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:my_family/models/member.dart';

class Utils {
  static Map<String, String> roles = {
    "HUSBAND": '丈夫',
    "WIFE": "妻子",
    "SON": "儿子",
    "DAUGHTER": "女儿",
  };
  static bool isPhoneNumber(String str) {
    return new RegExp(
            '^((13[0-9])|(15[^4])|(166)|(17[0-8])|(18[0-9])|(19[8-9])|(147,145))\\d{8}\$')
        .hasMatch(str);
  }

  static String roleToRolename(String role) {
    return roles[role];
  }

  static String rolenameToRole(String name) {
    String ret = '';
    roles.forEach((k, v) {
      if(v == name) {
        ret = k;
      }
    });
    return ret;
  }

  static List<String> rolenames() {
    List<String> list = List();
    roles.forEach((k, v) {
      list.add(v);
    });
    return list;
  }

  static String formatDate(int time) {
    if (time == null) {
      return "";
    } else {
      DateTime date = new DateTime.fromMillisecondsSinceEpoch(time);
      return "${date.year.toString()}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')} ${date.hour.toString().padLeft(2, '0')}:${date.minute.toString().padLeft(2, '0')}";
    }
  }

  static String formatDate2(int time) {
    if (time == null) {
      return "";
    } else {
      DateTime date = new DateTime.fromMillisecondsSinceEpoch(time);
      return "${date.year.toString()}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}";
    }
  }

  static Widget getMemberHeader(Member member) {
    if (member.avatar != null && member.avatar.length > 10) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(6),
        child: CachedNetworkImage(
          imageUrl: member.avatar,
          fit: BoxFit.fill,
          placeholder: (context, url) => CircularProgressIndicator(),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
      );
    } else if (member.familyRole == 'HUSBAND') {
      return Image.asset('images/dad.png');
    } else if (member.familyRole == 'WIFE') {
      return Image.asset('images/mum.png');
    } else if (member.familyRole == 'SON') {
      return Image.asset('images/boy.png');
    } else {
      return Image.asset('images/girl.png');
    }
  }

  static String formatDate3(int time) {
    if (time == null) {
      return "";
    } else {
      DateTime today = DateTime.now();
      DateTime date = new DateTime.fromMillisecondsSinceEpoch(time);
      if(today.year == date.year && today.month == date.month && today.day == date.day ) {
     
          return "今天 ${date.hour.toString().padLeft(2, '0')}:${date.minute.toString().padLeft(2, '0')}";
   
      } else {
        return "${date.year.toString()}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')} ${date.hour.toString().padLeft(2, '0')}:${date.minute.toString().padLeft(2, '0')}";
      }
      
    }
  }
}
