class ForumReply {
  String content;
  int created;
  int forumId;
  int id;
  int updated;
  int userId;
  String userNick;
  String userAvatar;

  ForumReply(
      {this.content,
      this.created,
      this.forumId,
      this.id,
      this.updated,
      this.userId,
      this.userNick, 
      this.userAvatar});

  ForumReply.fromJson(Map<String, dynamic> json) {
    content = json['content'];
    created = json['created'];
    forumId = json['forumId'];
    id = json['id'];
    updated = json['updated'];
    userId = json['userId'];
    userNick = json['userNick'];
    userAvatar = json['userAvatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['content'] = this.content;
    data['created'] = this.created;
    data['forumId'] = this.forumId;
    data['id'] = this.id;
    data['updated'] = this.updated;
    data['userId'] = this.userId;
    data['userNick'] = this.userNick;
    data['userAvatar'] = this.userAvatar;
    return data;
  }
}