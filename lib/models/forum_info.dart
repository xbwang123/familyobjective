class ForumInfo {
  String category;
  String content;
  int created;
  int id;
  int likes;
  String title;
  String userAvatar;
  int updated;
  int userId;
  String userNick;

  ForumInfo(
      {this.category,
      this.content,
      this.created,
      this.id,
      this.likes,
      this.title,
      this.updated,
      this.userId,
      this.userNick});

  ForumInfo.fromJson(Map<String, dynamic> json) {
    category = json['category'];
    content = json['content'];
    created = json['created'];
    id = json['id'];
    likes = json['likes'];
    title = json['title'];
    updated = json['updated'];
    userId = json['userId'];
    userNick = json['userNick'];
    userAvatar = json['userAvatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category'] = this.category;
    data['content'] = this.content;
    data['created'] = this.created;
    data['id'] = this.id;
    data['likes'] = this.likes;
    data['title'] = this.title;
    data['updated'] = this.updated;
    data['userId'] = this.userId;
    data['userNick'] = this.userNick;
    data['userAvatar'] = this.userAvatar;
    return data;
  }
}